 FROM node:argon
 
 RUN npm i express mongodb request -S
 RUN mkdir -p /usr/src/app/static
 WORKDIR /usr/src/app
 COPY ./src /usr/src/app/
 COPY ./static /usr/src/app/static

 
 CMD ["node", "server.js"]
