function checkEnabled() {
    return $("#themeOptions").val() && $("#wordsNr").val() >= 1 && $("#paragraphsNr").val() >= 1;

}


$(document).ready(function () {

    $("#result-container").slideUp();

    $("#themeOptions,#wordsNr,#paragraphsNr,#languageOption").on("change", function () {
        $("#generateLI").prop("disabled", !checkEnabled());
    }).trigger("change");

    $("#generateLI").click(function (event) {
        $(this).prop("disabled", true);

        $("#result-container").slideUp();
        $.ajax({
            url: "/get_paragraph",
            data: {
                theme: $("#themeOptions").val(),
                parSize: $("#wordsNr").val(),
                parCount: $("#paragraphsNr").val(),
                language: $("#languageOption").val(),
            },
            success: function (data) {
                if (typeof data !== "undefined") {
                    $("#result-container").slideDown();
                    $("#result").html('<div><p>' + data.join('</p><p>') + '</p></div>');
                } else {
                    $("#result").text("No text generated... mon");
                    $("#result-container").slideDown();
                }

            },
            complete: function () {
                $("#generateLI").prop("disabled", false);
            },
            error: function () {
                $("#result").text("No text generated... mon");
                $("#result-container").slideDown();
            }

        });

    });

    $("#themeOptions").change(function (event) {
        var theme;
        theme = $(this).val();

        if (theme === "nature") {
        $("#backgroundDiv").css("background", "url(https://upload.wikimedia.org/wikipedia/commons/3/36/Hopetoun_falls.jpg)");
	$("#backgroundDiv").css("background-size", "100%");                        
        }
        if (theme === "science") {
        $("#backgroundDiv").css("background", "url(http://www.readorrefer.us/media/article/articleTEVa9s13.png)");   
	$("#backgroundDiv").css("background-size", "100%");                     
        }
        if (theme === "history") {
        $("#backgroundDiv").css("background", "url(http://wsap.academy/wp-content/uploads/2015/03/1123676.jpg)");
	$("#backgroundDiv").css("background-size", "100%");                        
        }
        if (theme === "random") {
        $("#backgroundDiv").css("background", "url(https://s-media-cache-ak0.pinimg.com/originals/41/5f/a6/415fa6d6cf261782b455e99c226f8dac.jpg)");
	$("#backgroundDiv").css("background-size", "100%");            
        }
    });

});
