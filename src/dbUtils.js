var MongoClient = require('mongodb').MongoClient
var fs = require('fs')

mongo_ip = process.env.MONGO_HOST
const MONGO_URL = 'mongodb://' + mongo_ip + ':27017/test'
const MONGO_COLL = "paragraphs"

exports.dbExecute = function(func) {
    MongoClient.connect(MONGO_URL, function(err, db) {
      if (err) throw err;
      console.log("Connected to Database");

      func(db)
    });
}

function fileToWordArr(file) {
    return fs.readFileSync(file).toString().split(/\s+/g);
}

exports.initDatabase = function () {
    exports.dbExecute(function(db) {
        db.createCollection(MONGO_COLL, function(err, collection){
           if (err) throw err;

            collection.remove({});

            collection.insert({theme: 'nature', content: fileToWordArr('static/nature.txt')});
            collection.insert({theme: 'history', content: fileToWordArr('static/history.txt')});
            collection.insert({theme: 'science', content: fileToWordArr('static/science.txt')});

            console.log("Created collection " + MONGO_COLL);
        });
    });
}
