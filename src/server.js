var express = require('express')
var dbUtils = require('./dbUtils.js')
var translate = require('./translate.js')

var app = express()

const MONGO_COLL = "paragraphs"

dbUtils.initDatabase()

app.use(express.static('static'))

function getRandom(max) {
    return Math.floor(Math.random() * (max + 1));
}

function capitalizeFirst(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}

function stringContains(str, sub) {
    return str.indexOf(sub) >= 0;
}

function endSentence(str) {
    endings = ".!?";
    if (stringContains(endings, str.slice(-1))) {
        return str;
    }

    sentence = str
    if (stringContains(",/:;", str.slice(-1))) {
        sentence = str.slice(0, -1);
    }
    newEnd = endings.charAt(getRandom(endings.length-1));
    return sentence + newEnd;
}

function chooseParagraph(allWords, parSize) {
    origLen = Math.floor(allWords.length / 2);
    start = getRandom(origLen);
    chosenWords = allWords.slice(start, start + parSize);

    paragraph = chosenWords.join(" ");

    paragraph = capitalizeFirst(paragraph);
    return endSentence(paragraph)
}

function getParagraphs(allWords, parSize, count) {
    while (allWords.length < parSize) {
        allWords = allWords.concat(allWords)
    }
    allWords = allWords.concat(allWords);

    paragraphs = [];
    for (i = 0; i < count; i++) {
        paragraphs.push(chooseParagraph(allWords, parSize))
    }
    return paragraphs;
}

app.get('/get_paragraph', function (req, res) {
    var query = req.query;
    theme = query.theme;
    if (theme == 'random') {
        themes = ['science', 'nature', 'history'];
        randIndex = getRandom(themes.length - 1);
        theme = themes[randIndex]
    }

    dbUtils.dbExecute(function(db) {
        var the_coll = db.collection(MONGO_COLL)
        the_coll.findOne({'theme': theme}, function(err, doc) {
            if (err) {
                res.send("");
                return
            }

            var parCount = parseInt(query.parCount);
            var parSize = parseInt(query.parSize);
            var paragraphs = getParagraphs(doc.content, parSize, parCount);
            var language = query.language;
            if (language === 'English') {
                res.send(paragraphs);
            } else {
                translate.translate(paragraphs, language, function(translated_paragraphs){
                    res.send(translated_paragraphs)
                });
            }
        })
    });
})

app.listen(8080, function () {
  console.log('Starting the Troll App')
})
