var request = require('request')

LANG_MAPPINGS = {
    'English': 'en',
    'Spanish': 'es',
    'German': 'de',
    'French': 'fr'
}

exports.translate = function(text_arr, target_lang, cback) {
    API_KEY = 'AIzaSyBhNybAG8mtI0uzLpF3rqd8L5O0Hef5vHw';
    DELIM = '_____________';
    text = text_arr.join(DELIM)
    lang = LANG_MAPPINGS[target_lang];
    url = 'https://www.googleapis.com/language/translate/v2?key=' + API_KEY + '&q=' + text + '&source=en&target=' + lang;

    request(url, function (error, response, body) {
        response = JSON.parse(body);
        translated = response.data.translations[0].translatedText;
        translated_arr = translated.split(DELIM);
        cback(translated_arr);
    });
}
